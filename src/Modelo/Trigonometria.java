/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Santiago
 */
public class Trigonometria {

    /*Funcion seno, calcula el seno de un angulo por medio de la serie de Taylor, por lo cual pide como
      parametros el valor del angulo y el numero de iteraciones que debera realizar, entre mayor el numero 
      de iteraciones mas preciso el resultado*/
    public float seno(float x, int n) {
        float result = 0;
        for (int i = 0; i <= n; i++) {
            result += (Math.pow(-1, i) / facto((2 * i) + 1)) * Math.pow(Math.toRadians(x), ((2 * i) + 1));
        }
        return result;
    }
    
    /*Funcion coseno, calcula el coseno de un angulo por medio de la serie de Taylor, por lo cual pide como
      parametros el valor del angulo y el numero de iteraciones que debera realizar, entre mayor el numero 
      de iteraciones mas preciso el resultado*/
    public float coseno(float x, int n) {
        float result = 0;
        for (int i = 0; i <= n; i++) {
            result += (Math.pow(-1, i) / facto(2 * i)) * Math.pow(Math.toRadians(x), (2 * i));
        }
        return result;
    }

    /*El metodo facto calcula el fctorial de un numero que se le ingrese cmo parametro, retorna un float
      para evitar que el resultado aparesca como null*/
    private float facto(float n) {
        if (n == 0) {
            return 1;
        } else {
            return n * facto(n - 1);
        }
    }

}
