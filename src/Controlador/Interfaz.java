/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Trigonometria;
import java.util.Scanner;

/**
 *
 * @author Santiago
 */
public class Interfaz {

    Scanner sc = new Scanner(System.in);
    Trigonometria t = new Trigonometria();
    Validador v = new Validador();

    /*El metodo viw de la clase interfaz simplemente genera la interfaz con el usuario para que el pueda acceder a las funciones para clacualr el seno y el coseno*/
    public void view() {
        String x = "";
        String opc = "";
        String n = "";
        do {
            System.out.println("Funciones trigonometricas\n1)calcular Seno\n2)calcular coseno\n3)Salir\nIngrese su opcion: ");
            opc = sc.next();
            switch (opc) {

                case "1":
                    do {
                        System.out.println("Ingrese grados: ");
                        x = sc.next();
                        if (v.esNumerico(x)) {
                            System.out.println("Ingrese numero de iteraciones: ");
                            n = sc.next();
                            if (v.esNumerico(n)) {
                                System.out.println("Seno de " + x + " = " + t.seno(Float.parseFloat(x), Integer.parseInt(n)));
                            } else {
                                System.out.println("el valor ingresado no es un numero");
                            }
                        } else {
                            System.out.println("el valor ingresado no es un numero");
                        }
                    } while (v.esNumerico(x) != true || v.esNumerico(n) != true);
                    break;

                case "2":
                    do {
                        System.out.println("Ingrese grados: ");
                        x = sc.next();
                        if (v.esNumerico(x)) {
                            System.out.println("Ingrese numero de iteraciones: ");
                            n = sc.next();
                            if (v.esNumerico(n)) {
                                System.out.println("Coseno de " + x + " = " + t.coseno(Float.parseFloat(x), Integer.parseInt(n)));
                            } else {
                                System.out.println("el valor ingresado no es un numero");
                            }
                        } else {
                            System.out.println("el valor ingresado no es un numero");
                        }
                    } while (v.esNumerico(x) != true || v.esNumerico(n) != true);
                    break;

                case "3":
                    System.out.println("Hasta luego");
                    break;

                default:
                    System.out.println("El valor ingresado no es valido");
                    break;
            }
        } while (!opc.equals("3"));
    }

}
