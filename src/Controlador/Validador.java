/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

/**
 *
 * @author Santiago
 */
public class Validador {

    public boolean esNumerico(String cadena) {
        //https://www.lawebdelprogramador.com/foros/Java/1680183-ingresar-solo-letras-y-no-numeros.html
        //Recorremos cada caracter de la cadena y comprobamos si son letras.
        //Para comprobarlo, lo pasamos a mayuscula y consultamos su numero ASCII.
        //Si está fuera del rango 65 - 90, es que NO son letras.
        //Para ser más exactos al tratarse del idioma español, tambien comprobamos
        //el valor 165 equivalente a la Ñ

        for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.toUpperCase().charAt(i);
            int valorASCII = (int) caracter;
            if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90)) {
                return true; //Se ha encontrado un caracter que no es letra
            }
        }

        //Terminado el bucle sin que se haya retornado true, es que todos los caracteres son letras
        return false;
    }

}
